const fs = require('fs');

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
};

// Store the given data into data.json

function writeJSON(data) {

    let writeJSONPromise = new Promise((resolve, reject) => {
        fs.writeFile('./data.json', JSON.stringify(data), 'utf-8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve("Data.json written successfully");
            }
        });
    });
    
    return writeJSONPromise;

}

// Read the data from data.json

function readFile() {

    let readFilePromise = new Promise((resolve, reject) => {
        fs.readFile('./data.json', 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });

    return readFilePromise;

}

// Retrieve data for ids : [2, 13, 23]

function retrieveData(data) {

    let retrieveDataPromise = new Promise((resolve, reject) => {
        let parsedData = JSON.parse(data);
        let employeeData = (Object.entries(parsedData))[0][1];
        let idArray = [2, 13, 23];
        let employeesWithGivenId = employeeData.filter((item) => {
            return (idArray.includes(item.id));
        });
        fs.writeFile('./problem1.json', JSON.stringify(employeesWithGivenId), 'utf-8', (err) => {
            if(err) {
                reject(err);
            } else {
                resolve(employeeData);
            }
        });
    });
    
    return retrieveDataPromise;

}

// Group data based on companies

function groupData(data) {

    let groupDataPromise = new Promise((resolve, reject) => {
        let groupedData = data.reduce((accumulator, current) => {
            if(accumulator[current.company]) {
                accumulator[current.company] = [...accumulator[current.company], current];
            } else {
                accumulator[current.company] = [current];
            }
            return accumulator;
        }, {});
        fs.writeFile('./problem2.json', JSON.stringify(groupedData), 'utf-8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
    return groupDataPromise;
}

// Get all data for company Powerpuff Brigade

function getDataOfPowerpuffBrigade(data) {
    let getDataPromise = new Promise((resolve, reject) => {
        let PowerpuffBrigade = data.filter((item) => {
            return (item.company === "Powerpuff Brigade");
        });
        fs.writeFile('./problem3.json', JSON.stringify(PowerpuffBrigade), 'utf-8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
    return getDataPromise;
}

// Remove entry with id 2.

function removeEntry12(data) {
    let removePromise = new Promise((resolve, reject) => {
        let newData = data.map((item) => {
            if(item.id === 12) {
                delete item;
            } else {
                return item;
            }
        });
        fs.writeFile('./problem4.json', JSON.stringify(newData), 'utf-8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
    return removePromise;
}

// Sort data based on company name

function sortData(data) {
    let sortPromise = new Promise((resolve, reject) => {
        let sortedData = data.sort((first, second) => {
            if(first.company > second.company) {
                return 1;
            } else if (first.company === second.company) {
                if (first.id > second.id) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        });
        fs.writeFile('./problem5.json', JSON.stringify(sortedData), 'utf-8', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    });
    return sortPromise;
}

// add the birthday to the information

function addDOB(data) {
    
    let addDOBPromise = new Promise((resolve, reject) => {
        let employeeData = (Object.entries(data))[0][1];
        let addedDOBData = employeeData.map((item) => {
            item.birthday = Date();
            return item;
        });
        fs.writeFile('./problem7.json', JSON.stringify(addedDOBData), 'utf-8', (err) => {
            if(err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
    return addDOBPromise;
}

writeJSON(data)
.then(() => {
    console.log("Data successfully written");
    return readFile();
})
.then((data) => {
    console.log("Data successfully read")
    return retrieveData(data);
})
.then((data) => {
    console.log("Data with ID retrieved");
    return groupData(data);
})
.then((data) => {
    console.log("Data Grouped and Written");
    return getDataOfPowerpuffBrigade(data);
})
.then((data) => {
    console.log("Data of powerpuff brigade retrieved");
    return removeEntry12(data);
})
.then((data) => {
    console.log("Entry 12 deleted");
    return sortData(data);
})
.then(() => {
    console.log("Data sorted");
    return addDOB(data);
})
.then((data) => {
    console.log("Birthday Added");
})
.catch((err) => {
    console.log(err);
});

